package cz.cvut.fel.ts1;

public class Arutyvaz {
    public long factorial(int number) {
        return number <= 1 ? 1 : factorial(number) * number;
    }

}
 